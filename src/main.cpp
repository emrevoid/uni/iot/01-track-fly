/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #1: Track the Led Fly
   Author:
   - Andrea Casadei 800898
*/

#define EI_ARDUINO_INTERRUPTED_PIN

#include <EnableInterrupt.h>
#include <Arduino.h>

#define SEED_PIN A2
#define BUTTON_COUNT 4
#define B1 2
#define B2 3
#define B3 4
#define B4 5
#define LED_COUNT 4
#define L1 9
#define L2 10
#define L3 11
#define L4 12
#define Ls 8
#define POT_PIN A0
#define POT_SCALE 128
#define MSG_INIT "Welcome to the Track to Led Fly Game. Press Key T1 to Star"
#define MSG_DIFFICULTY(X) "Difficulty set to: " + (String)X
#define MSG_STARTGAME "Go!"
#define MSG_FLY_POS(X) "Tracking the fly Pos: " + (String)X
#define MSG_GAMEOVER(X) "Game Over - Score: " + X
#define DIFFULTY_SCALE (1 / 8.0)
#define STATE_INIT 0
#define STATE_WELCOME 1
#define STATE_GAMESTART 2
#define STATE_ROUNDSTART 3
#define STATE_WAIT_BUTTON 4
#define STATE_MOVEFLY 5
#define STATE_FLYCAUGHT 6
#define STATE_GAMEOVER 7
#define BASE_TMIN 8000L
#define K 2

int buttons[BUTTON_COUNT] = {B1, B2, B3, B4};
int leds[LED_COUNT] = {L1, L2, L3, L4};
int flyPos, btPressed;
int state = 0;
int score = 0;
int difficulty = 1;

uint64_t tMin, flyTime;
uint64_t lastActionTime = millis();
uint64_t timeGameStarted;

void setup()
{
  Serial.begin(9600);
  randomSeed(analogRead(SEED_PIN));
  pinMode(Ls, OUTPUT);
  pinMode(L1, OUTPUT);
  pinMode(L2, OUTPUT);
  pinMode(L3, OUTPUT);
  pinMode(L4, OUTPUT);
  reset();
}

void loop()
{
  switch (state)
  {
  case STATE_INIT:
    initGame();
    break;
  case STATE_WELCOME:
    welcome();
    break;
  case STATE_GAMESTART:
    gameStart();
    break;
  case STATE_ROUNDSTART:
    roundStart();
    break;
  case STATE_WAIT_BUTTON:
    waitButton();
    break;
  case STATE_MOVEFLY:
    moveFly();
    break;
  case STATE_FLYCAUGHT:
    flyCaught();
    break;
  case STATE_GAMEOVER:
    gameOver();
    break;
  }
}

void initGame()
{
  Serial.println(MSG_INIT);
  int potValue = (analogRead(POT_PIN) / POT_SCALE) + 1;
  if (potValue != difficulty)
  {
    difficulty = potValue;
    printDiff();
  }
  enableInterrupt(B1, startGame, RISING);
  state = STATE_WELCOME;
}

void printDiff()
{
  Serial.println(MSG_DIFFICULTY(difficulty));
}

void startGame()
{
  state = STATE_GAMESTART;
}

void welcome()
{
  int potValue = (analogRead(POT_PIN) / POT_SCALE) + 1;
  if (potValue != difficulty)
  {
    difficulty = potValue;
    printDiff();
  }
  digitalWrite(Ls, HIGH);
  delay(1000);
  digitalWrite(Ls, LOW);
  delay(1000);
}

void gameStart()
{
  Serial.println(MSG_STARTGAME);
  disableInterrupt(B1);
  for (int i = 0; i < BUTTON_COUNT; i++)
  {
    enableInterrupt(buttons[i], buttonPressed, RISING);
  }
  digitalWrite(Ls, LOW);
  score = 0;
  flyPos = (int)random(0, LED_COUNT);
  tMin = BASE_TMIN / difficulty;
  state = STATE_ROUNDSTART;
}

void buttonPressed()
{
  if (flyPos + 2 == arduinoInterruptedPin)
  {
    state = STATE_FLYCAUGHT;
  }
  else
  {
    state = STATE_GAMEOVER;
  }
}

void roundStart()
{
  moveFly();
  randTime();
  lastActionTime = millis();
  timeGameStarted = millis();
  state = STATE_WAIT_BUTTON;
}

void randTime()
{
  flyTime = (long)random(tMin, K * tMin);
}

void moveFly()
{
  digitalWrite(leds[flyPos], LOW);
  int r = (int)random(0, 2);
  if (r == 0)
  {
    flyPos -= 1;
  }
  else
  {
    flyPos += 1;
  }

  if (flyPos == -1)
  {
    flyPos = 3;
  }
  else
  {
    if (flyPos == 4)
    {
      flyPos = 0;
    }
  }
  digitalWrite(leds[flyPos], HIGH);
}

void waitButton()
{
  if (!(millis() - timeGameStarted < flyTime))
    state = STATE_GAMEOVER;
}

void flyCaught()
{
  score++;
  tMin -= (tMin * DIFFULTY_SCALE);
  Serial.println(MSG_FLY_POS((flyPos + 1)));
  state = STATE_ROUNDSTART;
}

void gameOver()
{
  Serial.println((String)MSG_GAMEOVER(score));
  reset();
}

void reset()
{
  for (int i = 0; i < BUTTON_COUNT; i++)
  {
    disableInterrupt(buttons[i]);
  }
  for (int i = 0; i < LED_COUNT; i++)
  {
    digitalWrite(leds[i], LOW);
  }
  digitalWrite(Ls, LOW);
  state = STATE_INIT;
}
